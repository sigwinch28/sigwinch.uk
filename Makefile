SRCDIR ?= ./pages
DSTDIR ?= ./build

SRCRES = $(STYLES)
RES = $(addprefix $(DSTDIR)/, $(SRCRES))

SRCS = $(wildcard $(SRCDIR)/*.md)
HTMLS = $(patsubst $(SRCDIR)/%.md, $(DSTDIR)/%.html, $(SRCS))

PANDOC ?= pandoc
PANDOCFLAGS ?= -f markdown -t html -s -c style.css -A _footer.html

.PHONY: clean all html res

all: html $(DSTDIR)/style.css

html: $(HTMLS)

clean:
	-rm $(HTMLS)
	-rm -rf $(DSTDIR)

$(DSTDIR)/style.css: style.css
	cp $< $@

$(DSTDIR):
	-mkdir $@

$(DSTDIR)/%.html: $(SRCDIR)/%.md $(FOOTER) $(STYLES) | $(DSTDIR)
	pandoc $(PANDOCFLAGS) -o $@ $<

