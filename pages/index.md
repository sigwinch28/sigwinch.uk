---
title: SIGWINCH (28)
---

Email is [joe@sigwinch.uk](mailto:joe@sigwinch.uk).  
Public key fingerprint is [`90C1 E93F ABF2 5FA3 E5F8 4F3B A858 E61A 29BA C8F0`](https://sigwinch.uk/.well-known/openpgpkey/hu/n4w4kuq9ejc3kmthngg8ccja7y5j8i97).  
Work homepage is [https://cs.kent.ac.uk/~jrh53](https://cs.kent.ac.uk/~jrh53).

# Personal accomplishments & accolades

* Used `pandoc` to make [this website](https://gitlab.com/sigwinch28/sigwinch.uk), from a markdown source
* 100m Rainbow Distance swimming certificate
* Voted «Remain»
* Own a copy of «The Art of Electronics»
* Been within 50m of Barack Obama, separated only by air

# PGP keys via WKD

WKD (the [Web Key Directory](https://www.ietf.org/id/draft-koch-openpgp-webkey-service-06.txt)
specification) allows you host your own PGP keys for domains you have
DNS control over.

It's worth noting that GnuPG will only import the UID for the domain you're querying. It seems to ignore all other UIDs.

To get my (infrequently used) key, run:

```
gpg --auto-key-locate clear,wkd --locate-keys joe@sigwinch.uk
```

There's a tutorial [on the GPG website](https://wiki.gnupg.org/WKDHosting).

# Other

* [watch list](./watch.html)
* [email setup](./mail.html)
* [chronological order of Jack Ryan books](./jack-ryan.html)
* [bike](./bike.html)
