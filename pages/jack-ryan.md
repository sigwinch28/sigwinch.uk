---
title: The Chronological Order of "A Jack Ryan Novel" Series
author: Joe Harrison
---

1. Patriot Games
2. Red Rabbit
3. The Hunt for Red October
4. The Cardinal of the Kremlin
5. Clear and Present Danger
6. The Sum of All Fears
7. Debt of Honor
8. Executive Orders
9. Command Authority
10. Full Force and Effect
11. Commander in Chief
