---
title: bike
---

In mid-2018 I purchased an (Elephant
Bike)[https://elephantbike.co.uk/] which is a refurbished Pashley
Mailstar, which is essentially a (Pashley
Pronto)[https://www.pashley.co.uk/bikes/carrier-cycles/pronto.php].

The Pashley Mailstar was used by Royal Mail until quite recently,
(when it was decided that the fleet would be
scrapped)[https://www.theguardian.com/environment/bike-blog/2013/dec/09/royal-mail-post-bikes].

The bike comes with (among other things):

* Front drum brake ((Sturmey Archer
  X-FD)[http://www.sturmey-archer.com/en/products/detail/x-fd])
* Rear drum brake and 3-speed hub ((Sturmey Archer
  X-RD3)[http://www.sturmey-archer.com/en/products/detail/x-rd3])
* (Schwalbe Marathon
  Plus)[https://www.schwalbe.com/en-GB/tour-reader/marathon-plus.html]
  tyres, which feature a retroreflective rim

I also purchased the following up-sells:

* Front basket frame, which attaches to the frame of the bike, not the
  handlebars.
* Plastic tub, as used by Royal Mail.

There were small bits of metal from the punching process left in the
front basket frame, which I removed using plenty of jiggling and
careful manipulation of a pairof long-nose pliers.

## Changes & Accessories

In the few months of owning the bicycle, I've made several changes to
it in the interests of performance, safety, and personal preference.


### Bell

I immediately purchased a large chrome "ding-dong" bell, which makes
a satisfyingly loud (but somewhat courteous) two-tone noise which
- in my opinion - greatly surpasses the little thumb-flick bells.
It's effective from a much greater range, and looks gorgeous.

### Brake Maintenance

The brakes on the bicycle felt a little soft, with occasional squeal
from the front brake. I disassembled both brakes to expose the pads,
cleaned them and the braking surface, and greased the sprung brake arm.
This resolved the braking and noise issues.

### Rattling in the Tub

The plastic tub rattles loudly when riding on anything but perfectly
smooth surfaces. I noticed that there are eight holes in the tub, in
pairs, which roughly line up with the bars of the basket frame
underneath. I used zip ties to secure the tub to the frame,
eliminating most of the noise.

The remainder of the sound was coming from items in the tub bouncing
against it. To solve this, I cut a piece of office
carpet (such as those available in tiles from B&Q) and zip-tied it
into the tub through the same holes.

### Lights

It is difficult to find a front light which fits the available
mounting system on the bicycle. The light could be mounted to the
handlebars, but I think the beam would be partially obscured by the
basket (if any). There is, however, a brazed-on mount on the right
fork suitable for an old incandescent battery-powered light. This is
obviously only suitable for countries with left-hand traffic.

I also found that modern battery-powered LED bicycle lights were
excessively priced. This seems unreasonable: LED cobs, suitable 
plastic optics, and rechargable lithium batteries (such as the 18650)
are readily available at little cost. Why then, do many bicycle lights
cost upwards of £50?

It seems that there is respite from the madness, though. Germany and
The Netherlands appear to be cycling havens, and their products
reflect this. I opted for a _Busch & Mueller Lyt T Senso Plus_ front
light, and a _Busch & Mueller Flat S Plus_ tail light. These lights
are dynamo powered, and were cheaply available in new condition from
online retailers based in Western Europe.

The only trouble is that the bicycle does not have a dynamo
pre-installed. I chose to have my front wheel rebuilt around a
_Sturmey Archer X-FDD_ combined drum brake and dynamo. Although the
tyres have a bottle dynamo strip, the weather resistance and low
maintenance of a hub dynamo appealed to me.

The front light came with a pre-installed cable with bare ends which
was easily installed into the dynamo. The rear light, however, came
without a cable, and without connectors. I have since purchased a
length of dynamo cable (essentially loudspeaker wire) and a pack of
2.8mm crimp spade connectors, including plastic covers.

The new lights are much better:

* Decreased risk of light theft - the lights are attached to the bike
  semi-permanently.
* Convenience - lights cannot be forgotten, nor do they require
  re-charging.
* Reduced maintenance - the front light can be affixed securely and
  tightly, ensuring the light does not move when bumped, moving the
  beam.
* Increased safety - I can either run my lights during the day or use
  the daytime sensor of the front light, essentially giving me daytime
  running lights. This offers increased visibility to other road users.

Furthermore, there is no noticeable drag from the dynamo when riding.
