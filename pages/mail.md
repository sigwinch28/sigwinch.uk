---
title: mail
---

On Debian testing:
```
sudo apt install isync notmuch{,-emacs}`
```

Example of a `~/.mbsyncrc` file:
```
IMAPAccount sigwinch
Host <imap.host.com>
User <user@host.com>
Pass <password>
# There are also tutorials for using GPG for storing passwords.
# This isn't a real concern for me as I use app passwords
SSLType IMAPS
AuthMechs LOGIN

IMAPStore sigwinch-remote
Account sigwinch

MaildirStore sigwinch-local
Path ~/.mail/sigwinch/
Inbox ~/.mail/sigwinch/INBOX
SubFolders Verbatim

Channel sigwinch
Master :sigwinch-remote:
Slave :sigwinch-local:
Patterns *
Create Both
SyncState *
```

Make the mail directories:
```
mkdir -p ~/.mail/sigwinch/`
```

Update mail:
```
mbsync -a
```

Index mail:
```
notmuch new
```

Update `~/.emacs`:

```
(autoload 'notmuch "notmuch" "notmuch mail" t)
```

Either run `M-x notmuch` or `emacs -f notmuch`. Done.
