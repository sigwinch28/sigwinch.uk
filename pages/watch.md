---
title: watch list
---

In no particular order:

* Pirates of Silicon Valley
* ~~Three Days of the Condor~~
* ~~Real Genius~~
* Steve Jobs
* City Slickers
* Incredibles 2
* ~~Upgrade~~
* ~~G.I. Jane~~
* ~~Baby Driver~~
* ~~A Quiet Place~~
* ~~Stripes~~
* ~~The Ides of March~~
* ~~Up in the Air~~
* ~~The Producers (remake)~~
* The Producers
* ~~Eyes Wide Shut~~
* ~~Blade Runner~~
* ~~Blade Runner 2049~~
